List<dynamic> sortReturn(List<dynamic> input, [int compare(dynamic a, dynamic b)?]) {
  List<dynamic> arr = input.map((c)=>c).toList();
  if (compare==null) {
    arr.sort();
  } else {
    arr.sort(compare);
  }
  return arr;
}
