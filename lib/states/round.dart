class Round {
  int id;
  int subrounds;
  late List<String> players;

  bool done = false;

  int cards;
  bool publicGuess = false;
  int scoreScale = 10;

  late List<List<int>> guesses;
  late List<List<int>> scores;

  Round(this.id, this.players, this.subrounds, this.cards, this.publicGuess, this.scoreScale);

  Round.fromJSON(Map<String, dynamic> json)
    : id = json['id'] as int,
      subrounds = json['subrounds'] as int,
      players = json['players'].cast<String>() as List<String>,
      done = json['done'] as bool,
      cards = json['cards'] as int,
      publicGuess = json['publicGuess'] as bool,
      scoreScale = json['scoreScale'] as int,
      guesses = json['guesses'].map((d)=>d.cast<int>()).toList().cast<List<int>>(),
      scores = json['scores'].map((d)=>d.cast<int>()).toList().cast<List<int>>();

  Map<String, dynamic> toJson() => {
    'id': id,
    'subrounds': subrounds,
    'players': players,
    'done': done,
    'cards': cards,
    'publicGuess': publicGuess,
    'scoreScale': scoreScale,
    'guesses': guesses,
    'scores': scores
    };
}

class RoundReturn {
  late List<List<int>> guesses;
  late List<List<int>> scores;

  RoundReturn(this.guesses, this.scores);
}
