import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:stichling/routes/game_activity.dart';
import '../components/expandable_card.dart';

class GameConfigActivity extends StatefulWidget {
  const GameConfigActivity({Key? key}) : super(key: key);

  @override
    State<GameConfigActivity> createState() => _GameConfigActivityState();
}
class _GameConfigActivityState extends State<GameConfigActivity> {
  List<String> players = [];
  int rounds = -1;
  bool publicGuess = true;
  int scoreScale = 10;
  int cards = -1;
  int subrounds = 0;
  late TextEditingController _controller;
  late TextEditingController _idController;
  @override
    void initState() {
      super.initState();
      _controller = TextEditingController();
      _idController = TextEditingController();
      _idController.text=DateTime.now().toIso8601String();
    }

  @override
    void dispose() {
      _controller.dispose();
      super.dispose();
    }

  @override
    Widget build(BuildContext ctx) {
      return Scaffold(
          appBar: AppBar(
            title: const Text("Neues Stichling Spiel"),
            leading: IconButton(
              icon: const Icon(FeatherIcons.arrowLeft, color: Colors.white),
              onPressed: () => Navigator.of(context).pop(),
              ),
            ),
          body: SingleChildScrollView(child:Center(child:Column(
                children: [
                ExpandableCard(
                  defaultExpand: true, 
                  title: "Spieler",
                  child: (players.isEmpty)?const Text("Du hast noch keine Spieler hinzugefügt.\n Drücke + zum erstellen"):SizedBox(height: players.length*50, child:ListView.builder(
                      itemCount: players.length,
                      itemExtent: 50,
                      shrinkWrap: true,
                      itemBuilder: (BuildContext context, int index) {
                      return ListTile(
                          title: Text(players[index]),
                          leading: const Icon(FeatherIcons.user),
                          trailing: IconButton(
                            onPressed: (){
                            setState(() {
                                players.removeAt(index);
                                });
                            }, 
icon: const Icon(FeatherIcons.minus)),
                          );
                      })),
icons: [
         IconButton(
             onPressed: () {
             showDialog(
                 context: ctx, 
                 builder: (BuildContext context) => AlertDialog(
                   title: const Text("Neuer Spieler"),
                   content:TextField(
                     controller: _controller,
                     autofocus: true,
                     onSubmitted: (name) {
                     setState(() {
                         players=[...players, name];
                         });
                     _controller.text="";
                     },
decoration: const InputDecoration(
              icon: Icon(FeatherIcons.user),
              hintText: 'Spielername'
              ),
),
                   actions: [
                   TextButton(
                     onPressed: (){
                     _controller.text="";
                     Navigator.of(context).pop();
                     }, 
child: const Text("Fertig")
),
                   TextButton(
                       onPressed: (){
                       setState(() {
                           players=[...players, _controller.text];
                           });
                       _controller.text="";
                       }, 
child: const Text("Hinzufügen")
)
                   ],
                   )
                     );
             }, 
icon: const Icon(FeatherIcons.plus))
        ],
        ),
        ExpandableCard(
            defaultExpand: false, 
            title: "Erweitert",
            child: Column(
              children: [
              ListTile(
                leading: const Icon(FeatherIcons.type),
                title: const Text("Spiel Name"),
                trailing: SizedBox(
                width: 100,
                height: 100,
                  child: TextField(
                  controller: _idController,
                )
                  ),
              ),
              ListTile(
                leading: const Icon(FeatherIcons.repeat),
                title: const Text("Runden anzahl"),
                trailing: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                  IconButton(
                    onPressed: (){
                    int r = (rounds>0)?rounds:players.length;
                    if (r == 1) {
                    setState(() {
                        rounds=-1;
                        });
                    } else {
                    setState(() {
                        rounds=r-1;
                        });
                    }
                    }, 
icon: const Icon(FeatherIcons.minus, color: Colors.grey)
),
                  Text((rounds<1)?players.length.toString():rounds.toString(), style: TextStyle(color: (rounds<1)?Colors.black:Colors.orange),),
                  IconButton(
                    onPressed: (){
                    int r = (rounds>0)?rounds:players.length;
                    setState(() {
                        rounds=r+1;
                        });
                    }, 
icon: const Icon(FeatherIcons.plus, color: Colors.grey,)
)
                    ],
                    ),
                    ),
                    ListTile(
                        leading: const Icon(FeatherIcons.eye),
                        title: const Text("öffentlich Raten"),
                        trailing: IconButton(onPressed: (){
                          setState(() {
                              publicGuess=!publicGuess;
                              });
                          }, icon: (publicGuess)?const Icon(FeatherIcons.checkSquare, color: Colors.blue,):const Icon(FeatherIcons.square))
                        ),
                    ListTile(
                        leading: const Icon(FeatherIcons.barChart2),
                        title: const Text("Punkte für korrekte Antwort"),
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                          IconButton(
                            icon: const Icon(FeatherIcons.minus),
                            onPressed: () {
                            if (scoreScale - 10 >= 0) {
                            setState(() {
                                scoreScale-=10;
                                });
                            }
                            },
                            ),
                          Text(scoreScale.toString()),
                          IconButton(
                            icon: const Icon(FeatherIcons.plus),
                            onPressed: () {
                            setState(() {
                                scoreScale+=10;
                                });
                            },
                            ),
                          ],
                          ),
                          ),
                          ListTile(
                            leading: const Icon(FeatherIcons.alignJustify),
                            title: const Text("Kartenanzahl"),
                            trailing: DropdownButton(
                              items: [
                                const DropdownMenuItem(
                                  child: Text("Auto"),
                                  value: -1
                                  ),
                                DropdownMenuItem(
                                  child: Text((16*1).toString()),
                                  value: 16*1
                                  ),
                                DropdownMenuItem(
                                  child: Text((16*2).toString()),
                                  value: 16*2
                                  ),
                                DropdownMenuItem(
                                  child: Text((16*3).toString()),
                                  value: 16*3
                                  ),
                                DropdownMenuItem(
                                  child: Text((16*4).toString()),
                                  value: 16*4
                                  )
                              ], 
                              icon: const Icon(FeatherIcons.edit),
                              value: cards,
                              onChanged: (int? v) {
                                setState(() {
                                                                  cards=v!;
                                                                });
                                }
                              ),
                          ),
                          ListTile(
                          leading: const Icon(FeatherIcons.clipboard),
                          title: const Text("zusätzliche Reihen pro Runde"),
                          trailing: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              IconButton(
                                icon: const Icon(FeatherIcons.minus),
                                onPressed: (){
                                  if (subrounds>0) {
                                    setState(() {
                                                                          subrounds-=1;
                                                                        });
                                    }
                                  },
                              ),
                              Text(subrounds.toString()),
                              IconButton(
                                icon: const Icon(FeatherIcons.plus),
                                onPressed: (){
                                  setState(() {
                                                                      subrounds+=1;
                                                                    });
                                  },
                              )
                            ],
                          ),
                          )
                            ],
                            )
                              )
                              ],
                              )),
                              ),
                              floatingActionButton: FloatingActionButton(
                                  onPressed: () {
                                  if (players.length > 1) {
                                  Navigator.push(ctx, 
                                      MaterialPageRoute(
                                        builder: (BuildContext ctx ) => GameActivity(
                                          players: players,  
                                          rounds: rounds, 
                                          publicGuess: publicGuess,
                                          scoreScale: scoreScale,
                                          cards: cards,
                                          subrounds: subrounds.toInt()+players.length,
                                          id: _idController.text
                                          )));
                                  }
                                  },
tooltip: 'Los',
backgroundColor: (players.length<2)?Colors.grey:Colors.blue,
child: const Icon(FeatherIcons.play),
), // This trailing comma makes auto-formatting nicer for build methods.
                              );
    }
}
