import 'dart:collection';

import 'package:stichling/states/round.dart';

List<int> calcScores({
    required List<List<int>> guesses,
    required List<List<int>> scores,
    required int scoreScale,
    }) {
  List<int> builder = [];
  int len = (guesses[0].length+scores[0].length)~/2;

  builder = List.generate(
      len,
      (index) {
      int rlen = (guesses.length+scores.length)~/2;
      List<int> pGuesses = guesses.map((e) => e[index]).toList();
      List<int> pScores = scores.map((e) => e[index]).toList();

      int temp = 0;

      for (int i = 0; i<rlen; i++) {
      if (pGuesses[i]==pScores[i] && pGuesses[i] >= 0) {
      temp+=scoreScale+pScores[i];
      }
      }

      return temp;
      });

  return builder;
}

HashMap<String, int> calcAllScores({
    required List<Round> roundData,
    required int index
    }) {
  HashMap<String, int> builder = HashMap<String, int>();
  roundData[0].players.forEach((element) {builder[element]=0;});

  for (int round = 0; round<index;round++) {
    Round dt = roundData[round];
    if (dt.done) {
    List<List<int>> s = dt.scores;
    List<List<int>> g = dt.guesses;
    List<String> p = dt.players;
    List<int> c = calcScores(
        guesses: g, 
        scores: s, 
        scoreScale: dt.scoreScale);
    for (int i = 0; i<p.length; i++) {
      String player = p[i];
      int score = c[i];
      builder[player]=builder[player]!+score;
    }
    }
  }

  return builder;
}
