import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stichling/routes/game_review_activity.dart';
import 'package:stichling/states/round.dart';
import 'game_config_activity.dart';

class MainActivity extends StatefulWidget {
  const MainActivity({Key? key}) : super(key: key);

  @override
    State<MainActivity> createState() => _MainActivityState();
}
class _MainActivityState extends State<MainActivity> {
  Set<String> hist = Set();
  late SharedPreferences prefs;
  @override
    void initState() {
      super.initState();
      ()async {
        prefs = await SharedPreferences.getInstance();
        setState(() {
            hist = prefs.getKeys();
            });
      }();
    }
  @override
    void reassemble() {
      super.reassemble();
      ()async {
        prefs = await SharedPreferences.getInstance();
        setState(() {
            hist = prefs.getKeys();
            });
      }();
    }
  @override
    Widget build(BuildContext ctx) {
      return Scaffold(
          appBar: AppBar(
            title: const Text("Stichling"),
            leading: const Icon(FeatherIcons.archive),
            ),
          body: Center(
            child: (hist.isNotEmpty)?ListView.builder(
              itemCount: hist.length,
              itemBuilder: (BuildContext ctx, int index) {
              return ListTile(
                  leading: const Icon(FeatherIcons.command),
                  title: Text(hist.elementAt(index)),
                  trailing: const Icon(FeatherIcons.chevronRight),
                  onLongPress: (){
                  showDialog(
                      context: ctx, 
                      builder: (BuildContext ctx) => AlertDialog(
                        title: Text(hist.elementAt(index)),
                        actions: [
                        TextButton.icon(
                          onPressed: ()async {
                          await prefs.remove(hist.elementAt(index));
                          setState(() {
                              hist = prefs.getKeys();
                              });
                          Navigator.of(ctx).pop();
                          }, 
icon: const Icon(FeatherIcons.delete), 
label: const Text("Löschen")),

                        TextButton.icon(
                          onPressed: (){
                          Navigator.of(ctx).pop();
                          }, 
icon: const Icon(FeatherIcons.x), 
label: const Text("Abbruch"))
                        ],
                        )
                          );
                  },
onTap: (){
         List<dynamic> m = jsonDecode(prefs.getString(hist.elementAt(index))!).cast<Map<String, dynamic>>();
         List<Round> rounds = m.map<Round>((d)=>Round.fromJSON(d)).toList();

         Navigator.of(ctx).push(
             MaterialPageRoute(
               builder: (BuildContext ctx) => GameReviewActivity(
                 id: hist.elementAt(index),
                 rounds: rounds
                 )
               )
             );
       },
       );
              }
      ):const Text("Du hast noch keine Spiele gespielt")
        ),
        floatingActionButton: FloatingActionButton(
            onPressed: () {
            Navigator.push(ctx, 
                MaterialPageRoute(builder: (ctx)=>const GameConfigActivity()));
            },
tooltip: 'Neues Spiel',
child: const Icon(FeatherIcons.plus),
), // This trailing comma makes auto-formatting nicer for build methods.
        );
    }
}
