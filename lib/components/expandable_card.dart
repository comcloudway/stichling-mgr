import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';

class ExpandableCard extends StatefulWidget {
  const ExpandableCard({
      Key? key, 
      required this.defaultExpand,
      required this.title,
      required this.child,
      this.icons=const []
      }) : super(key: key);

  final bool defaultExpand;
  final String title;
  final Widget child;
  final List<Widget> icons;

  @override
    State<ExpandableCard> createState() => _ExpandableCardState();
}
class _ExpandableCardState extends State<ExpandableCard> {
  bool expanded = false;
  @override
    void initState() {
      super.initState();
      expanded=widget.defaultExpand;
    }
  @override
    Widget build(BuildContext ctx) {
      return Card(
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              children: [
              Row(
                children: [
                Text(
                  widget.title,
                  style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                Row(children: [
                  ...widget.icons,
                  IconButton(
                    onPressed: (){
                    setState(() {
                        expanded=!expanded;
                        });
                    },
icon: Icon((expanded)?FeatherIcons.chevronDown:FeatherIcons.chevronRight),
)])
                ],
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                ),
            (expanded)?widget.child:Column(children: const [],)
              ],
              ),)
                );
    }
}
