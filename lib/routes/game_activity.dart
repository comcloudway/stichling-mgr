import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stichling/functions/scores.dart';
import 'package:stichling/routes/game_review_activity.dart';
import 'package:stichling/routes/round_activity.dart';
import 'package:stichling/states/round.dart';

class GameActivity extends StatefulWidget {
  const GameActivity({
      Key? key,
      required this.players,
      required this.rounds,
      required this.publicGuess,
      required this.scoreScale,
      required this.cards,
      required this.subrounds,
      required this.id,
      }) : super(key: key);

  final List<String> players;
  final int rounds;
  final bool publicGuess;
  final int scoreScale;
  final int cards;
  final int subrounds;
  final String id;

  @override
    State<GameActivity> createState() => _GameActivityState();
}
class _GameActivityState extends State<GameActivity> {
  int round = 0;
  late List<Round> roundData;

  @override
    void initState() {
      super.initState();
      int r = (widget.rounds>0)?widget.rounds:widget.players.length;

      roundData = List.generate(
          r,
          (index) {
          Round round = Round(
              index, 
              [...widget.players.sublist(index), ...widget.players.sublist(0, index)], 
              widget.subrounds,
              (widget.cards>0)?widget.cards:(widget.players.length <= 4)?widget.players.length*12:48,
              widget.publicGuess,
              widget.scoreScale
              );
          return round;
          }
          );
    }

  @override
    Widget build(BuildContext ctx) {
      return Scaffold(
          appBar: AppBar(
            leading: IconButton(
              onPressed: () {
              Navigator.of(ctx).pop();
              },
tooltip: "Verlassen ohne Speichern",
icon: const Icon(FeatherIcons.x)),
            title: Text("Rundenübersicht: " + widget.id),
            ),
          body: SingleChildScrollView(child:Center(
              child:Stepper(
                currentStep: round,
                controlsBuilder: <Widget>(BuildContext ctx, ControlsDetails cd) {
                return Row(
                    children: [
                    TextButton(
                      onPressed: (){
                      if (!roundData[round].done) {
                      Navigator.of(ctx).push(MaterialPageRoute(
                            builder: (context) => RoundActivity(
                              roundData: roundData[round],
                              onDone: (RoundReturn ret) {
                              setState(() {
                                  roundData[round].scores=ret.scores;
                                  roundData[round].guesses=ret.guesses;
                                  roundData[round].done=true;
                                  });
                              },
                              )
                            )
                          );
                      }
                      }, 
child: const Text("Spielen"),
style: ButtonStyle(
  backgroundColor: MaterialStateProperty.all((roundData[round].done)?Colors.transparent:Colors.blue),
  foregroundColor: MaterialStateProperty.all((roundData[round].done)?Colors.grey:Colors.white)
  ),
),
TextButton(
    onPressed: () async {
    if (roundData[roundData.length-1].done) {
    //save
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(widget.id, jsonEncode(roundData));
    Navigator.of(ctx).push(
        MaterialPageRoute(
          builder: (BuildContext ctx) => GameReviewActivity(
            id: widget.id,
            rounds: roundData
            )
          )
        );
    } else {
    if (roundData[round].done) {
    cd.onStepContinue!();
    }
    }
    }, 
child: Text((roundData[roundData.length-1].done)?"Beenden":"Weiter"),
       style: ButtonStyle(
           backgroundColor: MaterialStateProperty.all((!roundData[round].done)?Colors.transparent:Colors.blue),
           foregroundColor: MaterialStateProperty.all((!roundData[round].done)?Colors.grey:Colors.white)
           ),
       )
         ].map((b)=>Padding(
               padding: const EdgeInsets.all(20),
               child: b
               )).toList(),
         );
                },
onStepContinue: (){
                  setState(() {
                      round++;
                      });
                },
steps: [
         ...roundData.map((roun) => Step(
               title: Text("Runde " + (roun.id + 1).toString()),
               isActive: round==roun.id,
               content: Container(
                 alignment: Alignment.centerLeft,
                 child:Column(
                   mainAxisAlignment: MainAxisAlignment.start,
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                   ListTile(
                     leading: const Icon(FeatherIcons.users),
                     title: Text("Spieleranzahl: " + roun.players.length.toString()),
                     ),
                   ListTile(
                     leading: const Icon(FeatherIcons.list),
                     title: Text("Spieler Reihenfolge: " + roun.players.join(', '))
                     ),
                   DataTable(
                     columns: [
                     const DataColumn(label: Text("Name")),
                     ...List.generate(
                       round+1,
                       (index) => DataColumn(
                         label: Text((index+1).toString(), 
                           style: TextStyle(
                             color: (index==round)?Colors.blue:Colors.black
                             ),
                           )
                         )
                       ),
                     const DataColumn(label: Text("Gesamt")),
                     ], 
                     rows: List.generate(
                       roundData[roun.id].players.length, 
                       (player) => DataRow(
                         cells: [
                         DataCell(Text(roundData[roun.id].players[player])),
                         ...List.generate(round+1, (index) {
                           if (roundData[index].done) {
                           return DataCell(Text(calcScores(
                                   guesses: roundData[index].guesses, 
                                   scores: roundData[index].scores,
                                   scoreScale: roundData[index].scoreScale
                                   )[roundData[index].players.indexOf(roundData[round].players[player])].toString()));
                           } else {
                           return const DataCell(Text("..."));
                           }
                           }),
                         DataCell(
                           Text(
                             calcAllScores(roundData: roundData, index: (roun.done)?round+1:round)[roun.players[player]].toString()
                             )
                           )
                         ]
                         )
                         )
                         )
                         ],
                         ))
                           )).toList(),
                         ]
                           )))
                           );
    }
}

