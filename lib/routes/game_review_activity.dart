import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stichling/components/expandable_card.dart';
import 'package:stichling/functions/scores.dart';
import 'package:stichling/functions/sort.dart';
import 'package:stichling/states/round.dart';

class GameReviewActivity extends StatefulWidget {
  GameReviewActivity({
      Key? key,
      required this.id,
      required this.rounds
      }) : super(key: key);

  final String id;
  final List<Round> rounds;

  @override
    State<GameReviewActivity> createState() => _GameReviewAxtivityState();
}
class _GameReviewAxtivityState extends State<GameReviewActivity> {
  @override
    Widget build(BuildContext ctx) {
      return WillPopScope(
      onWillPop: ()async{
              Navigator.of(ctx).pushNamed("/");
              return false;
        },
      child: Scaffold(
          appBar: AppBar(
          title: Text(widget.id),
            leading: IconButton(
              onPressed: (){
              Navigator.of(ctx).pushNamed("/");
              },
icon: const Icon(FeatherIcons.arrowLeft)),
            ),
          body: SingleChildScrollView(
            child: Center(
              child: Column(
                children: (widget.rounds.isNotEmpty)?[
                ExpandableCard(
                  defaultExpand: false, 
                  title: "Details", 
                  child: Column(
                  mainAxisSize: MainAxisSize.min,
                    children: [
                      ListTile(
                        leading: const Icon(FeatherIcons.type),
                        title: const Text("Rundenanzahl"),
                        trailing: Text(widget.rounds.length.toString()),
                      ),
                      ListTile(
                        leading: const Icon(FeatherIcons.eye),
                        title: const Text("öffentliches Raten"),
                        trailing: Icon(widget.rounds[0].publicGuess?FeatherIcons.checkSquare:FeatherIcons.square),
                      ),
                      ListTile(
                        leading: const Icon(FeatherIcons.barChart2),
                        title: const Text("Punkte für korrekte Antwort"),
                        trailing: Text(widget.rounds[0].scoreScale.toString()),
                      ),
                      ListTile(
                        leading: const Icon(FeatherIcons.alignJustify),
                        title: const Text("Kartenanzahl"),
                        trailing: Text(widget.rounds[0].cards.toString()),
                      ),
                      ListTile(
                        leading: const Icon(FeatherIcons.clipboard),
                        title: const Text("zusätzliche Reihen pro Runde"),
                        trailing: Text(widget.rounds[0].subrounds.toString()),
                      )
                    ],
                  )
                  ),
                ExpandableCard(
                  defaultExpand: true, 
                  title: "Spieler", 
                  child: ListView.builder(
                  shrinkWrap: true,
                    itemCount: widget.rounds[0].players.length,
                    itemBuilder: (BuildContext ctx, int index) => ListTile(
                      leading: const Icon(FeatherIcons.user),
                      title: Text(widget.rounds[0].players[index])
                    ),
                    )
                  ),
                ExpandableCard(
                  defaultExpand: true, 
                  title: "Endergebnis",
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                    ...sortReturn(
                      calcAllScores(
                        roundData: widget.rounds, 
                        index: widget.rounds.length
                        ).entries.toList(),
                        (a,b) {
                          int sa = a.value;
                          int sb = b.value;
                          if (sa==sb) {
                            return 0;
                            } else if (sa<sb) {
                              return 1;
                            } else {
                              return -1;
                            }
                          }
                        ).asMap().map((i, e)=>MapEntry(i, ListTile(
                          leading: Text((i+1).toString() + '.'),
                          title: Text(e.key),
                          trailing: Text(e.value.toString()),
                        ))).values.toList()
                    ],
                    )
                  ),
                ...widget.rounds.map(
                  (round) => ExpandableCard(
                    defaultExpand: false,
                    title: "Runde " + (round.id+1).toString(),
                    child: DataTable(columns: [
                      const DataColumn(label:Text("Name")),
                      ...List.generate(
                        round.subrounds,
                        (i)=>DataColumn(label:Text((i+1).toString()))),
                      const DataColumn(label:Text("Punkte"))
                    ], rows: calcScores(
                        guesses: round.guesses, 
                        scores: round.scores, 
                        scoreScale: round.scoreScale)
                        .asMap().map((player, res) => MapEntry(player, DataRow(
                          cells: [
                            DataCell(Text(round.players[player])),
                            ...List.generate(
                              round.subrounds, 
                              (i) {
                                int g = round.guesses[i].cast<int>()[player];
                                int s = round.scores[i].cast<int>()[player];
                                bool co = false;
                                int calc = 0;
                                calc=round.scoreScale+g;
                                if (g == s) {
                                  co=true;
                                }
                                return DataCell(
                                  Text(
                                    calc.toString(),
                                    style: TextStyle(
                                      decoration: (!co)?TextDecoration.lineThrough:TextDecoration.none
                                    )
                                    )
                                  );
                                  }
                              ),
                            DataCell(Text(res.toString()))
                          ]
                          ))).values.toList())
                    )
                    )
                  
                  ]:[
                  const Text("Keine Spieldaten vorhanden")
                  ]),
                  ),
                  )
                    ));
    }
}
