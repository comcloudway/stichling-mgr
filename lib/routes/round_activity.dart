import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:stichling/functions/scores.dart';
import 'package:stichling/states/round.dart';

class RoundActivity extends StatefulWidget {
  RoundActivity({
      Key? key,
      this.onDone,
      required this.roundData}) : super(key: key);

  Round roundData;
  late ValueChanged<RoundReturn>? onDone;

  @override
    State<RoundActivity> createState() => _RoundActivityState();
}

enum Moves {
  Guess,
  Score,
  Next
}

class _RoundActivityState extends State<RoundActivity> {
  int run = 0;
  Moves move = Moves.Guess;
  int questionaireIndex = 0;

  List<List<int>> guesses = [];
  List<List<int>> scores = [];

  @override
    Widget build(BuildContext ctx) {
      return Scaffold(
          appBar: AppBar(
            leading: IconButton(
              onPressed: () {
              Navigator.of(ctx).pop();
              },
icon: const Icon(FeatherIcons.arrowLeft)
),
            title: Text("Runde " + (widget.roundData.id+1).toString()),
            ),
          body: Center(
            child: SingleChildScrollView(
              child: DataTable(columns: [
                const DataColumn(label: Text("Name")),
                ...List.generate(widget.roundData.subrounds, (index) => DataColumn(
                    label: Column(
                      children: [
                      Text((index+1).toString(), style: TextStyle(
                          color: (index==run)?Colors.blue:Colors.black,
                          fontWeight: (index==run)?FontWeight.bold:FontWeight.w300
                          )),
                      Text("K/P: " + (index+1).toString(),
                        style: TextStyle(
                          fontWeight: (index==run)?FontWeight.w300:FontWeight.w200,
                          fontSize: 10
                          )
                        )
                      ],
                      mainAxisSize: MainAxisSize.min,
                      )
                    )
                  ),
                const DataColumn(label: Text("Punkte"))
                  ], rows: List.generate(
                      widget.roundData.players.length, 
                      (index) => DataRow(
                        cells: [
                        DataCell(Text(widget.roundData.players[index], 
                            style: TextStyle(
                              color: (run%(widget.roundData.subrounds~/(widget.roundData.subrounds~/widget.roundData.players.length))==index)?Colors.blue:Colors.black
                              )
                            )
                          ),
                        ...(List.generate(widget.roundData.subrounds, (r) {
                            int score = -1;
                            int calc = -1;
                            int guess = -1;

                            if (r + 1 <= guesses.length) {
                            if (index +1 <= guesses[r].length) {
                            guess=guesses[r][index];
                            }
                            }
                            if (r + 1 <= scores.length) {
                            if (index + 1 <= scores[r].length) {
                            score = scores[r][index];
                            }
                            }

                            if (score != -1 && guess != -1) {
                            calc = guess + widget.roundData.scoreScale;
                            }

                            return DataCell(
                                Column(
                                  children: [
                                  Text((guess<0)?"?":guess.toString(),
                                    style: const TextStyle(
                                      fontWeight: FontWeight.w200,
                                      ),),
                                  Text((calc<0)?"??":calc.toString(),
                                    style: TextStyle(
                                      decoration: (score==guess)?TextDecoration.none:TextDecoration.lineThrough
                                      )),
                                  Text((score<0)?"?":score.toString(),
                                    style: const TextStyle(
                                      fontWeight: FontWeight.w200,
                                      ),),
                                  ],)
                                );
                        }).toList()),
                        DataCell(Text((scores.isNotEmpty&&guesses.isNotEmpty)?calcScores(
                                guesses: guesses, 
                                scores: scores, 
                                scoreScale: widget.roundData.scoreScale
                                )[index].toString():"..."))
                          ]
                          )
                          ).toList()
                          )
                          )
                          ),
                        floatingActionButton: FloatingActionButton(
                            onPressed: () {
                            if (move == Moves.Score) {
                            if (scores.length<=run) {
                            scores.add(List.generate(widget.roundData.players.length, (index) => 0));
                            }
                            showDialog(
                                context: ctx,
                                barrierDismissible: false,
                                builder: (BuildContext ctx) => ScoreAlert(
                                  start:run%(widget.roundData.subrounds~/(widget.roundData.subrounds~/widget.roundData.players.length)),
                                  end:run%(widget.roundData.subrounds~/(widget.roundData.subrounds~/widget.roundData.players.length)),
                                  roundData: widget.roundData,
                                  guesses: guesses[run],
                                  run: run,
                                  onDone: (g) {
                                  setState(() {
                                      scores[run]=g;
                                      move=Moves.Next;
                                      });
                                  },
                                  )
                                );
                            } else if (move == Moves.Guess) {
                              if (guesses.length<=run) {
                                guesses.add(List.generate(widget.roundData.players.length, (index) => 0));
                              }
                              showDialog(
                                  context: ctx,
                                  barrierDismissible: false,
                                  builder: (BuildContext ctx) => GuessAlert(
                                    start:run%(widget.roundData.subrounds~/(widget.roundData.subrounds~/widget.roundData.players.length)),
                                    end:run%(widget.roundData.subrounds~/(widget.roundData.subrounds~/widget.roundData.players.length)),
                                    roundData: widget.roundData, 
                                    run: run,
                                    onDone: (g) {
                                    setState(() {
                                        guesses[run]=g;
                                        move=Moves.Score;
                                        });
                                    },
                                    )
                                  );
                            } else if (move == Moves.Next) {
                              if (run+1==widget.roundData.subrounds) {
                                // end round
                                RoundReturn ret = RoundReturn(guesses, scores);
                                widget.onDone!(ret);
                                Navigator.of(ctx).pop();
                              } else {
                                setState(() {
                                    run++;
                                    move=Moves.Guess;
                                    });
                              }
                            }
                            },
child: Icon((move==Moves.Score)?FeatherIcons.type:(move==Moves.Guess)?FeatherIcons.italic:(run+1==widget.roundData.subrounds)?FeatherIcons.check:FeatherIcons.chevronRight),
       ),
       );
    }
}

class GuessAlert extends StatefulWidget {
  GuessAlert({
      Key? key,
      required this.roundData,
      required this.run,
      required this.start,
      required this.end,
      this.onDone
      }) : super(key: key);

  Round roundData;
  int run;
  int start;
  int end;
  late ValueChanged<List<int>>? onDone;

  @override
    State<GuessAlert> createState() => _GuessAlertState();
}
class _GuessAlertState extends State<GuessAlert> {
  late int questionaireIndex;
  late int start;
  late int end;
  late List<bool> showGuess;
  late List<int> guesses;

  @override
    void initState() {
      super.initState();
      questionaireIndex=widget.start;
      end=widget.end;
      start=widget.start;
      showGuess = List.generate(widget.roundData.players.length, (index) => widget.roundData.publicGuess);
      guesses = List.generate(widget.roundData.players.length, (index) => 0);
    }

  @override
    Widget build(BuildContext ctx) {
      return AlertDialog(
          title: Text(widget.roundData.players[questionaireIndex] + " schätze deine Punkte (" + guesses[questionaireIndex].toString() + "/" + (1+widget.run).toString() +")"),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
            Slider(
              label: guesses[questionaireIndex].toString(), 
              value: guesses[questionaireIndex].toDouble(), 
              min: 0,
              max: (1+widget.run).toDouble(),
              divisions: (1+widget.run),
              onChanged: (v) {
              setState(() {
                  guesses[questionaireIndex]=v.toInt();
                  });
              }),
            ListTile(
              leading: const Icon(FeatherIcons.eye),
              title: const Text("Anderen meine Punkte zeigen"),
              trailing: IconButton(
                onPressed: (){
                setState(() {
                    showGuess[questionaireIndex]=!showGuess[questionaireIndex];
                    });
                }, 
icon: (showGuess[questionaireIndex])?const Icon(FeatherIcons.checkSquare, color: Colors.blue,):const Icon(FeatherIcons.square)
),
              ),
            (showGuess[questionaireIndex])?DataTable(
                columns: widget.roundData.players.map((e) => DataColumn(label: Text(e, style: TextStyle(fontWeight: (e==widget.roundData.players[questionaireIndex])?FontWeight.bold:FontWeight.w300),))).toList(), 
                rows: [
                DataRow(cells: List.generate(
                    widget.roundData.players.length, 
                    (index) => DataCell(Text(guesses[index].toString())))
                  )
                ]
                ):const Text("Wenn du anderen deine Punkte nicht zeigst, kannst du sie auch nicht einsehen")
              ]),
              actions: [
                TextButton(
                    child: const Text('Zurück'),
                    onPressed: (){
                    if (questionaireIndex == start) {
                    Navigator.of(ctx).pop();
                    } else {
                    if (questionaireIndex>0) {
                    setState(() {
                        questionaireIndex-=1;
                        });
                    } else {
                    setState(() {
                        questionaireIndex=end;
                        });
                    }
                    }
                    },
                    ),
              TextButton(
                  onPressed: () {
                  if (questionaireIndex+1==end||(end==0&&questionaireIndex+1>=widget.roundData.players.length)) {
                  widget.onDone!(guesses);
                  Navigator.of(ctx).pop();
                  } else {
                  if (questionaireIndex+1>=widget.roundData.players.length) {
                  setState(() {
                      questionaireIndex=0;
                      });
                  } else {
                  setState(() {
                      questionaireIndex+=1;
                      });
                  }
                  }
                  }, 
child: const Text("Weiter")
)
                ],
                );
    }
}
class ScoreAlert extends StatefulWidget {
  ScoreAlert({
      Key? key,
      required this.roundData,
      required this.run,
      required this.guesses,
      required this.start,
      required this.end,
      this.onDone
      }) : super(key: key);

  Round roundData;
  List<int> guesses;
  int run;
  int start;
  int end;
  late ValueChanged<List<int>>? onDone;

  @override
    State<ScoreAlert> createState() => _ScoreAlertState();
}
class _ScoreAlertState extends State<ScoreAlert> {
  late int questionaireIndex;
  late int end;
  late int start;
  late List<int> scores;

  @override
    void initState() {
      super.initState();
      questionaireIndex=widget.start;
      start=widget.start;
      end=widget.end;
      scores = List.generate(widget.roundData.players.length, (index) => widget.guesses[index]);
    }

  @override
    Widget build(BuildContext ctx) {
      return AlertDialog(
          title: Text(widget.roundData.players[questionaireIndex] + " gib deine erreichten Punkte an (" + scores[questionaireIndex].toString() + "/" + (1+widget.run).toString() +")"),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
            Text("Du wolltest " + widget.guesses[questionaireIndex].toString() + " Punkte"),
            Slider(
              label: scores[questionaireIndex].toString(), 
              value: scores[questionaireIndex].toDouble(), 
              min: 0,
              max: (1+widget.run).toDouble(),
              divisions: (1+widget.run),
              onChanged: (v) {
              setState(() {
                  scores[questionaireIndex]=v.toInt();
                  });
              }),
            ]),
          actions: [
          TextButton(
            child: const Text('Zurück'),
            onPressed: (){
            if (questionaireIndex == start) {
            Navigator.of(ctx).pop();
            } else {
            if (questionaireIndex>0) {
            setState(() {
                questionaireIndex-=1;
                });
            } else {
            setState(() {
                questionaireIndex=end;
                });
            }
            }
            },
            ),
        TextButton(
            onPressed: () {
            if (questionaireIndex+1==end||(end==0&&questionaireIndex+1>=widget.roundData.players.length)) {
            widget.onDone!(scores);
            Navigator.of(ctx).pop();
            } else {
            if (questionaireIndex+1>=widget.roundData.players.length) {
            setState(() {
                questionaireIndex=0;
                });
            } else {
            setState(() {
                questionaireIndex+=1;
                });
            }
            }


            }, 
child: const Text("Weiter")
)
],
);
    }
}
